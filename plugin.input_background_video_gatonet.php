<?php

/*
Plugin Name: Video Background Gatonet
Plugin URI: http://gatonet.de/
Description: Enable Video Background.
Version: 1.0
Author: Gatonet
Author URI: http://gatonet.de/
Type: global
Hook: system_extension_bgvid
Function: input_bgvid:input_interface
End

Plugin Name: Video Background Gatonet
Type: global
Hook: system_uploader_link
Function: input_bgvid:input_link
End

Plugin Name: Video Background Gatonet
Type: front
Hook: pre_load
Function: input_bgvid:bigvidfront_output
End


*/



class input_bgvid
{

/** So far so good.
We create a input link so that the user can click on
the icon in the backend. The function is called from the description parser...
hope I get this right.
**/
	function input_link()
	{
		$OBJ =& get_instance();
		global $go;
		return "<a href='?a=system&q=extend&x=bgvid&id=$go[id]' rel=\"facebox;height=300;width=400;modal=true\"  style=\"
		background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgaGVpZ2h0PSIxMDAwIiB3aWR0aD0iMTAwMCI+PHBhdGggZD0iTTAgNDk5Ljk2OHEwIC0yMDcuMDE4IDE0Ni40NzUgLTM1My40OTN0MzUzLjQ5MyAtMTQ2LjQ3NSAzNTMuNDkzIDE0Ni40NzUgMTQ2LjQ3NSAzNTMuNDkzIC0xNDYuNDc1IDM1My40OTMgLTM1My40OTMgMTQ2LjQ3NSAtMzUzLjQ5MyAtMTQ2LjQ3NSAtMTQ2LjQ3NSAtMzUzLjQ5M3ptMjAxLjE1OSAyMjYuNTQ4bDU5Ny42MTggMGwwIC00NTMuMDk2bC01OTcuNjE4IDBsMCA0NTMuMDk2em03NC4yMTQgLTc0LjIxNGwwIC0zMDQuNjY4bDQ0OS4xOSAwbDAgMzA0LjY2OGwtNDQ5LjE5IDB6bTE1OC4xOTMgLTQ4LjgyNWwxNzUuNzcgLTEwMS41NTYgLTE3NS43NyAtMTAxLjU1NmwwIDIwMy4xMTJ6IiBmaWxsPSIjMDAwMDAwIi8+PC9zdmc+)
    ;
		padding: 0;
    width: 20px;
    height: 19px;
    background-size: contain;
    display: inline-block;
    background-repeat: no-repeat;
    line-height: 15px;
\"   ></a>";
	}


/** Now we create a interface for the popup box
this function is called by the description parser too...
**/

	function input_interface()
	{
		$OBJ =& get_instance();
		global $go;$pvid ="";
		// fire function on form submit
		if (isset($_POST['bgvidurl'])){ $this->submit_bgvid();
			$pvid = $_POST['bgvidurl'];
		}else{
			$pvid = $OBJ->db->fetchRecord("SELECT * FROM ".PX."objects
				WHERE id = '$go[id]'
				");
				$pvid = $pvid['vidbg'];
		}

		//load_helper('html'); <-- I donÂ´t know what this does
		//load_module_helper('files', $go['a']); <-- same here...
		$OBJ->template->pop_location = $OBJ->lang->word('Background video from Youtube');
		$OBJ->template->pop_links[] = array($OBJ->lang->word('close'), '#', "onclick=\"parent.faceboxClose(); return false;\"");

		$body = "Please input the youtube video id that you want to show on your site.<br>http://www.youtube.com/watch?v=<span style='color: red;'>UTPBDnHkVOI</span>&...";
		$body .= "<form  id='mainForm' method='post' enctype='multipart/form-data' action=''><input name='bgvidurl' value='".$pvid."'><input type='submit'></form>";
		$OBJ->template->body = $body;
		$OBJ->template->output('popup');
		exit;
	}
/** Final function that will submit
our video url to the database
*/
		function submit_bgvid(){
			global $go;
			$OBJ =& get_instance();

			$result = $OBJ->db->query("SHOW COLUMNS FROM ".PX."objects LIKE 'vidbg'");
			$exists = (mysql_num_rows($result))?TRUE:FALSE;
			$pvid = "";
				if(isset($_POST['bgvidurl'])){
					$pvid = $_POST['bgvidurl'];
				}
				if($exists && $pvid != ""){
							$OBJ->db->updateRecord("UPDATE ".PX."objects SET vidbg = '$pvid' WHERE id = '$go[id]'");
				}else{
						//print_r("Sorry, the plugin did not find the db column . Please close the box and try again.");
						$OBJ->db->query("ALTER TABLE ".PX."objects ADD vidbg VARCHAR(255)");
						$OBJ->db->updateRecord("UPDATE ".PX."objects SET vidbg = '$pvid' WHERE id = '$go[id]'");
					}



		}


		function bigvidfront_output(){
			$OBJ =& get_instance();
			$pvid = $OBJ->vars->exhibit['vidbg'];
				if($pvid != ""){
					$OBJ->page->exhibit['dyn_css'][] = "#tubular-container{ z-index:-1 !important } #tubular-shield{ z-index:-1 !important }";
					$OBJ->page->exhibit['append_page'][] = "<script src='//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js' type='text/javascript'></script><script type='text/javascript' src='https://cdn.jsdelivr.net/jquery.tubular/1.0.1/jquery.tubular.1.0.js'></script><script type='text/javascript'>$('document').ready(function() { var options = { videoId: '".$pvid."', start: 0, wrapperZIndex: -2 };$('#bgvidwrapper').tubular(options);});</script><div id='bgvidwrapper'></div>";
			/*
TASKS: Find a method to check if jquery is already in template.


				$OBJ->page->exhibit['append_page'][] = ""
."<script> \r\n"
."if(!window.jQuery){ \r\n"
."	var jqscript = document.createElement('script'); \r\n"
."	jqscript.type = 'text/javascript'; \r\n"
."	jqscript.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'; \r\n"
."	document.getElementsByTagName('head')[0].appendChild(jqscript); console.log('jquery appended');} \r\n"
."</script>\r\n"
."<script type='text/javascript'>var scriptloaded = '0';var timouts ='';\r\n"

."function startscript(){  console.log('startscript fired')\r\n"
."	var options = { videoId: '".$pvid."', start: 0, wrapperZIndex: -2 };\r\n"
."	 $('#bgvidwrapper').tubular(options); console.log('firsttest'); };    \r\n"

."function checkfunction(){ \r\n"
."	if(window.jQuery && scriptloaded == 1){ clearTimeout(timouts);\r\n"
."		console.log('firsttest');clearTimeout(timouts); console.log('ok we got script');startscript();\r\n"
."	}else{ if(scriptloaded == 0 && window.jQuery){ scriptloaded = '1';"
	."	var cdnscript = document.createElement('script'); \r\n"
	."	cdnscript.type = 'text/javascript'; \r\n"
	."	cdnscript.src = 'https://cdn.jsdelivr.net/jquery.tubular/1.0.1/jquery.tubular.1.0.js';\r\n"
	."	document.getElementsByTagName('head')[0].appendChild(cdnscript); console.log('script appended'); }\r\n"
."	console.log('no'); timouts = setTimeout(checkfunction, 4000);  } } \r\n"
."	checkfunction();\r\n"
."</script>\r\n"
."	<div id='bgvidwrapper'></div>\r\n";

*/




				}
		}




}
?>
